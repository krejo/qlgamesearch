//
//  DetailSegmentedViewController.swift
//  GBGameSearch
//
//  Created by JOSEPH KERR on 3/11/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// -----------------------------------------------------------------------------
///
///  Details of a Game resource
///  Consists of two views
///  1. A Description view with pic and detail text
///  2. Screen shots
///  Select a view use segmented control
///
// -----------------------------------------------------------------------------

import UIKit

protocol DetailSegmentedDelegate: class {
    func detailSegmented(_ controller:DetailSegmentedViewController, selected index: Int)
}


class DetailSegmentedViewController: UIViewController {
    
    // MARK: UI Elements
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var leftContainerView: UIView!
    @IBOutlet weak var rightContainerView: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!

    // MARK: - Properties
    
    /// Model to drive detail view
    var model: DetailModel?

    /// Provide updates
    weak var delegate: DetailSegmentedDelegate?
    
    /// Search service data handler
    weak var searchService:SearchServiceProtocol?

    /// Indicate whether images details are currently being retrieved
    var retrievingImagesDetails = false

    /// Number of images total in detail
    var imageCountTotal = 0
    
    /// Number of images processed
    var imageCountProcessed = 0

    /// Indicate whether to use preloaded images
    var preloadImages = false
    
    /// The segment index to start with
    var startSelectedSegment = 0
    
    /// Reference to collectionView
    weak var imagesCollection: ImagesCollectionViewController?

    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = model?.nameTitle {
            title = name
        }
        
        segmentedControl.selectedSegmentIndex = startSelectedSegment
        segmentValueChanged(segmentedControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: Action
    
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.leftContainerView.isHidden = false
            self.rightContainerView.isHidden = true
        } else
            if sender.selectedSegmentIndex == 1 {
                self.leftContainerView.isHidden = true
                checkToRetrieveImagesDetails()
                self.rightContainerView.isHidden = false
        }
        
        delegate?.detailSegmented(self, selected: sender.selectedSegmentIndex)
    }

    
    // MARK: operations
    
    /// Check to retrieve images
    func checkToRetrieveImagesDetails() {
        // ignore if already in the process of retrieving
        if !retrievingImagesDetails {
            if let _ = model?.imagesDetails {
                // already have the data
            } else {
                if let resource =  model?.resource_id {
                    retrieveImagesDetails(for: resource)
                }
            } 
        }
    }
    
    /// Retrieve details for resource
    /// - parameter resource: id of resource
    func retrieveImagesDetails(for resource: Int) {
        activity.startAnimating()
        searchService?.searchServiceDetailsImagesFor(resource) { (success, result) in
            if success {
                if let images = result["images"] as? [[String:Any]] {
                    self.model?.imagesDetails = images
                    self.imagesCollection?.imagesDetailsData = images

                    if self.preloadImages {
                        self.processDetailsImages()
                    } else {
                        DispatchQueue.main.async {
                            self.imagesCollection?.collectionView!.reloadData()
                            self.activity.stopAnimating()
                        }
                    }
                }
            }
            
            self.retrievingImagesDetails = false
        }

    }

    /// Obtain the images from urls
    func processDetailsImages() {
        processImagesDetails() {
            if let images = self.model?.images {
                self.imagesCollection?.imagesPreLoaded = images
            }
            DispatchQueue.main.async {
                self.imagesCollection?.collectionView!.reloadData()
                self.activity.stopAnimating()
            }
        }
    }
    
    /// Process the images retrieveing and adding to the list
    func processImagesDetails(completion: @escaping () -> Void) {
        if let imagesDetails = model?.imagesDetails {
            let image_specifier: String
            if self.view.traitCollection.horizontalSizeClass == .regular &&
                self.view.traitCollection.verticalSizeClass == .regular {
                image_specifier = "super_url"
            } else {
                image_specifier = "medium_url"
            }
            
            let imageURLs = imagesDetails.flatMap { $0[image_specifier] as? String }.flatMap { URL(string: $0) }
            imageCountTotal = imageURLs.count
            for imageURL in imageURLs {
                DispatchQueue.global().async {
                    if let imageData = try? Data(contentsOf: imageURL),
                        let image = UIImage(data: imageData) {
                        //print(image.size)
                        self.model?.images.append(image)
                        self.imageCountProcessed += 1
                    } else {
                        self.imageCountProcessed += 1
                    }
                    
                    if self.imageCountProcessed < self.imageCountTotal {
                        // still processing
                    } else {
                        completion()
                    }
                } // dispatch
            } // for each images
        } // let images
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "EmbedDescription" {
            //if let detail = segue.destination as? DetailSegmentedViewController,
            if let detail = segue.destination as? GameDetailViewController {
                detail.model = model
                // Pass the same searchService on
                detail.searchService = self.searchService
            }
        }
        else if segue.identifier == "EmbedScreen" {
            if let imagesDetail = segue.destination as? ImagesCollectionViewController {
                // keep a reference
                imagesCollection = imagesDetail
            }
        }
    }

}
