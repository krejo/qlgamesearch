//
//  SearchResults+UIViewControllerPreviewing.swift
//  GBGameSearch
//
//  Created by Joe Ker on 4/18/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//


import UIKit

extension SearchResultsTableViewController: UIViewControllerPreviewingDelegate {
    // MARK: UIViewControllerPreviewingDelegate
    
    /// Create a previewing view controller to be shown at "Peek".
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        print(#function)
        
        // Obtain the index path and the cell that was pressed.
        guard let indexPath = tableView.indexPathForRow(at: location),
            let cell =    tableView.cellForRow(at: indexPath) else {
                return nil
        }
        
        // Create a detail view controller and set its properties.
        guard let detailViewController = storyboard?.instantiateViewController(withIdentifier: "GameDetail") as? GameDetailViewController else {
            return nil
        }
        
        
        let model = detailModelObjectForItemAt(indexPath.row)
        detailViewController.model = model
        detailViewController.searchService = self.searchService
        detailViewController.peeking = true
        
        /*
         Set the height of the preview by setting the preferred content size of the detail view controller.
         Width should be zero, because it's not used in portrait.
         */
        //detailViewController.preferredContentSize = CGSize(width: 0.0, height: previewDetail.preferredHeight)
        
        // Set the source rect to the cell frame, so surrounding elements are blurred.
        previewingContext.sourceRect = cell.frame
        
        self.peekIndexPath = indexPath
        
        return detailViewController
    }
    
    /// Present the view controller for the "Pop" action.
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        
        print(#function)
        
        // Same as if segment selected.
        if let indexPath = peekIndexPath {
            self.detailLastSelectedSegmentIndex = nil // UnRemeber selected segment
            
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            self.performSegue(withIdentifier: "ShowDetail", sender: self)
        }
    }
}


// func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
//        //show(viewControllerToCommit, sender: self)
