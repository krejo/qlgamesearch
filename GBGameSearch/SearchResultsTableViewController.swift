//
//  GameResultsTableViewController.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/3/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// -----------------------------------------------------------------------------
///
///  Search results are presented.  A delegate is used to obtain images from
///  URLs
///
// -----------------------------------------------------------------------------

import UIKit

class SearchResultsMoreCell: UITableViewCell {
    @IBOutlet weak var activity: UIActivityIndicatorView!
}

protocol SearchTableDelegate: class {
    func resultsTable(_ controller:SearchResultsTableViewController, imageFor url:String) -> UIImage?
}


/// Search results list
class SearchResultsTableViewController: UITableViewController, DetailSegmentedDelegate {
    
    /// The model results
    var searchResults: [[String:Any]] = []
    
    /// Delegate to obtain image icons
    weak var delegate:SearchTableDelegate?

    /// Search service data handler
    weak var searchService:SearchServiceProtocol?

    /// Placeholder image when none available
    lazy var imageUnavailable = UIImage(named: "imageUnavailable")
    
    /// Search term used to generate results
    var searchTerm: String?
    
    /// Next page of results if desired
    var nextPage = 0
    
    /// The total reaults of search
    var totalResults = 0

    /// Indicates whether a nextPage results is in progress
    var moreSearching = false
    
    /// A lock for image updates and search results
    let lock = NSLock()

    /// Remember the segment index
    var detailLastSelectedSegmentIndex: Int?
    
    /// View for section header
    lazy var sectionHeaderView = UITableViewHeaderFooterView()
    
    /// The currently peeking indexPath
    var peekIndexPath: IndexPath?
    
    // MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let searchTerm = searchTerm {
            title = searchTerm
        } else {
            title = "Results"
        }
        let view = UIView()
        view.backgroundColor = UIColor.clear
        sectionHeaderView.backgroundView = view
        sectionHeaderView.contentView.backgroundColor = UIColor.init(white: 1.0, alpha: 0.90)
        
        
        // Check for force touch feature, and add force touch/previewing capability.
        if traitCollection.forceTouchCapability == .available {
            /*
             Register for `UIViewControllerPreviewingDelegate` to enable
             "Peek" and "Pop".
             (see: SearchResults+UIViewControllerPreviewing.swift)
             
             The view controller will be automatically unregistered when it is
             deallocated.
             */
            registerForPreviewing(with: self, sourceView: self.view)
            print("registerForPreviewing 3D Touch")
        }
        else {
            print("3D Touch Not Available")
        }

    }


    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sectionHeaderView
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // If already searching do nothing
        if !moreSearching,
            let moreCell = cell as? SearchResultsMoreCell,
            let searchTerm = searchTerm {
            DispatchQueue.main.async {
                moreCell.activity.startAnimating()
            }
            moreSearching = true
            
            searchService?.searchServiceResultsFor(nextPage, searchTerm: searchTerm) { (success, results, nextPage) in
                if success {
                    self.moreResults(results: results, nextPage: nextPage)
                } else {
                    self.moreSearching = false
                    DispatchQueue.main.async {
                        moreCell.activity.stopAnimating()
                    }
                }
            } // trailing
        }
    }
    

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Total Items: \(self.totalResults)"
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // plus one if there are more results to be retrieved
        if nextPage > 0 {
            return searchResults.count + 1
        }
        return searchResults.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if nextPage > 0 && indexPath.row == searchResults.count  {
            cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath)
        } else  {
            cell = resultCellForRowAt(indexPath)
        }
        return cell
    }
    
    
    // MARK: TableView Cells
    
    func resultCellForRowAt(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultsCell", for: indexPath)
        // Configure the cell...
        var primaryText = ""
        var detailText = ""
        
        let item = searchResults[indexPath.row]
        
        if let name = item["name"] as? String {
            primaryText = name
        }
        
        if let description = item["deck"] as? String {
            detailText = description
        }
        
        if let iconUrl = iconUrlString(for: item) {
            cell.imageView?.image = delegate?.resultsTable(self, imageFor: iconUrl)
        } else {
            cell.imageView?.image = imageUnavailable
        }
        
        cell.textLabel?.text = primaryText
        cell.detailTextLabel?.text = detailText
        
        return cell
    }

    
    // MARK: DetailSegmentedDelegate

    func detailSegmented(_ controller:DetailSegmentedViewController, selected index: Int) {
        detailLastSelectedSegmentIndex = index
    }

    
    // MARK: Helpers
    
    /// Retrieve the url string for icon
    func iconUrlString(for item:[String:Any]) -> String? {
        return (item["image"] as? [String:Any])?["icon_url"] as? String
    }
    /// Retrieve the url string for image
    func imageUrlString(for item:[String:Any]) -> String? {
        return (item["image"] as? [String:Any])?["small_url"] as? String
    }
    /// Retrieve the url string for large image
    func largeImageUrlString(for item:[String:Any]) -> String? {
        return (item["image"] as? [String:Any])?["medium_url"] as? String
    }

    
    // MARK: Updates
    
    /// Perform image update having the url
    func imageUpdate(for urlString: String) {
        // Lock for image updates
        lock.lock(); defer { lock.unlock() }
        performImageUpdate(for: urlString) { cell in
            let image = delegate?.resultsTable(self, imageFor: urlString)
            DispatchQueue.main.async {
                cell.imageView?.image = image
            }
        }
    }

    /// Perform image update having the url, with image
    func imageUpdate(for urlString: String, with image:UIImage) {
        // Lock for image updates
        lock.lock(); defer { lock.unlock() }
        performImageUpdate(for: urlString) { cell in
            DispatchQueue.main.async {
                cell.imageView?.image = image
            }
        }
    }

    /// Apply the image to any visible cells with URL
    func performImageUpdate(for urlString: String, completion: (UITableViewCell) -> Void) {
        
        if let visible = self.tableView.indexPathsForVisibleRows {
            for indexPath in visible {
                if indexPath.row < searchResults.count {  // maybe more cell
                    let item = searchResults[indexPath.row]
                    if let iconURL = iconUrlString(for: item), iconURL == urlString {
                        // Update in place
                        if let cell = self.tableView.cellForRow(at: indexPath) {
                            completion(cell)
                        }
                    }
                }
            }
        }
    }
    
    /// Appends additional searchResults and provides a nextPage
    func moreResults(results: [[String:Any]], nextPage:Int) {
        // Lock for image updates
        lock.lock(); defer { lock.unlock() }
        searchResults.append(contentsOf: results)
        self.nextPage = nextPage
        moreSearching = false  // no longer searching
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    
    // MARK: Detail model object
    
    /// Create new detail model object
    func detailModelObjectForItemAt(_ index:Int) -> DetailModel {
        let item = searchResults[index]
        let model = DetailModel()
        
        // Uncomment to allow image animations for screen shots
        model.imagesAnimationEnabled = true
        
        if let identifier = item["id"] as? Int {
            model.resource_id = identifier
        }
        if let name = item["name"] as? String {
            model.nameTitle = name
        }
        if self.view.traitCollection.horizontalSizeClass == .regular &&
            self.view.traitCollection.verticalSizeClass == .regular {
            if let imageUrl = largeImageUrlString(for: item) {
                model.imageURL = URL(string: imageUrl)
            }
        } else {
            if let imageUrl = imageUrlString(for: item) {
                model.imageURL = URL(string: imageUrl)
            }
        }
        if let iconUrl = iconUrlString(for: item) {
            model.startImage = delegate?.resultsTable(self, imageFor: iconUrl)
        }
        
        return model
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowDetail" {
            if let detail = segue.destination as? DetailSegmentedViewController,
                let selected = tableView.indexPathForSelectedRow {
                let model = detailModelObjectForItemAt(selected.row)
                
                detail.delegate = self
                detail.model = model
                // Pass the same searchService on
                detail.searchService = self.searchService
                
                if let selectedIndex = detailLastSelectedSegmentIndex {
                    detail.startSelectedSegment = selectedIndex
                }
            }
        }
    }
    
 
}


//    ["small_url", "thumb_url", "super_url", "icon_url", "medium_url", "tiny_url", "screen_url"]
// small_url
//    (556.0, 640.0)
//    (551.0, 640.0)
// medium_url 800 600
//    (835.0, 960.0)
//    (824.0, 960.0)
//    (759.0, 960.0)
// super_url
//    (536.0, 616.0)
//    (1012.0, 1280.0)
//    (639.0, 820.0)



