//
//  GameDetailViewController.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/3/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// -----------------------------------------------------------------------------
///
///  Detail view of an object from search Results
///  Contains a webview for description and Imageview for higher resolution 
///  image
///
// -----------------------------------------------------------------------------

import UIKit


/// Detail view of single game
class GameDetailViewController: UIViewController, UIWebViewDelegate {

    // MARK: UI Elements
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var detailActivity: UIActivityIndicatorView!
    
    /// Model to drive detail view
    var model: DetailModel?

    /// Search service data handler
    weak var searchService:SearchServiceProtocol?

    /// Indicate whether images details are currently being retrieved
    var retrievingImagesDetails = false
    
    /// Whether in peek or not
    var peeking = false
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
         When peeking the webView description is not shown
         */
       
        if peeking {
            self.webView.isHidden = true
        } else {
            webView.delegate = self
            loadHTMLIfAvailable()
        }
        
        if let image = model?.startImage {
            self.imageView.image = image
             model?.startImage = nil
        }
        
        if let name = model?.nameTitle {
            title = name
        }
        
        updateImage()
        
        if !peeking {
            startupDetailData()
        }
 
    }
    
    deinit {
        webView.delegate = nil
        //print("\(#function) Detail")
    }
    
    
    /// Retrieve detail info for the resource
    func startupDetailData() {
        if let resource =  model?.resource_id {
            // Get Description
            detailActivity.startAnimating()
            searchService?.searchServiceDetailsDescriptionFor(resource) { (success, result) in
                if success {
                    self.model?.details = result
                    self.processDetailsDescription()
                }
                DispatchQueue.main.async {
                    self.detailActivity.stopAnimating()
                }
            }
        }
    }
    
    
    // MARK: Actions

    @IBAction func didTap(_ sender: UITapGestureRecognizer) {
        self.tapAction()
    }
    
    
    /// Perform the tap action hide and unhide description
    func tapAction() {
        if  (model?.imageTapped)! {
            // UNZOOM
            imageView.stopAnimating()
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.webView.isHidden = false
            }, completion: { (fini) in
            })
            self.model?.imageTapped = false
        } else {
            // ZOOM
            zoomImage()
            UIView.animate(withDuration: 0.25, delay: 0.10, options: .curveEaseOut, animations: {
                self.webView.isHidden = true
            }, completion: { (fini) in
            })
            self.model?.imageTapped = true
        }
    }
    
    /// Zoom animation for the imageView
    func zoomImage() {
        let t = CGAffineTransform(scaleX: 1.6, y: 1.6)
        UIView.animate(withDuration: 0.30, delay: 0, options: [.curveEaseInOut], animations: {
            self.imageView.transform = t
        }) { (fini) in
            if fini {
                UIView.animate(withDuration: 0.20, delay: 0, options: [.curveEaseOut], animations: {
                    self.imageView.transform = CGAffineTransform.identity
                }) { (fini) in
                }
            }
            
        }
    }
    
    
    // MARK: UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true  // just so load HTML works
    }

    
    // MARK: Updates

    /// Obtain the image from the url stored property
    func updateImage() {
        if let imageURL = model?.imageURL {
            if imageView.image == nil {
                activity.startAnimating()
            }
            DispatchQueue.global().async {
                if let imageData = try? Data(contentsOf: imageURL),
                    let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self.imageView.image = image
                        self.activity.stopAnimating()
                    }
                }
            } // dispatch
        }
    }

    /// Load the html string if it exists
    func loadHTMLIfAvailable() {
        if let html = model?.htmlDescription {
            DispatchQueue.main.async {
                self.webView.loadHTMLString(html, baseURL: nil)
            }
        }
    }
    
    
    // MARK: Details

    func processDetailsDescription() {
        if let description = model?.details["description"] as? String {
            model?.htmlDescription = description
            loadHTMLIfAvailable()
        } else {
            // Go tapped when no description
            self.model?.imageTapped = false
            self.tapAction()
        }
    }
 

}


//print(Array(images[0].keys))
// images
// small_url
// 80 80
// medium_url
// 480 270
// super_url
// 640 360

