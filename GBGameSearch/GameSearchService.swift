//
//  GameSearchService.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/3/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// -----------------------------------------------------------------------------
///
///  An object that will create a service request, submit it
///  and wait for the repsonse.
///  searchResults(_:  will not block while waiting for response
///
// -----------------------------------------------------------------------------

import Foundation

/// A Search service to obatin results for a search term
class GameSearchService {

    /// Service response errors
    enum SearchServiceError {
        case responseError(NSError)
        case statusCodeInvalid(Int)
        case parsingError(String)
        case unrecognizedDataFormat
        case missingDataError
        case missingDataElementError(String)
        case apiStatusCode(Int,String)
    }
    
    /// field_list items for details
    enum SearchServiceDataKey: String {
        case id
        case name
        case deck
        case description
        case image
        case images
    }

    // MARK: Properties
    
    private let apiKey = "ad59511e47a20c10dfcecb643954dde2e3bd48c0"  //48c0
    private let baseURLString =  "http://www.giantbomb.com/api"
    

    /// A completionHandler for dataTask returns (results, error)
    func processResponse(_ data: Data?,_ response: URLResponse?,_ error: Error?) -> (result:[String:Any], serviceError:SearchServiceError?) {
        
        var resultData = [String:Any]()
        
        guard let data = data, error == nil else {
            let serviceError: SearchServiceError
            if let error = error {
                serviceError = .responseError(error as NSError)
            } else {
                serviceError = .missingDataError
            }
            return (resultData, serviceError)
        }
        
        // We have data and no error
        var status = 0
        if let httpResponse = response as? HTTPURLResponse {
            status = httpResponse.statusCode
        }
        
        var serviceErrorIfAny: SearchServiceError?
        
        if 200...299 ~= status {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let jsonResponse = json as? [String:Any] {
                    if let apiStatusCode = jsonResponse["status_code"] as? Int {
                        if apiStatusCode == 1 {
                            // Ok
                            resultData = jsonResponse
                            
                        } else if let apiError = jsonResponse["error"] as? String {
                            serviceErrorIfAny = .apiStatusCode(apiStatusCode, apiError)
                        } else {
                            serviceErrorIfAny = .apiStatusCode(apiStatusCode, "apiError \(apiStatusCode)")
                        }
                    } else {
                        serviceErrorIfAny = .missingDataElementError("status_code")
                    }
                } else {
                    serviceErrorIfAny = .unrecognizedDataFormat
                }
            } catch {
                serviceErrorIfAny = .parsingError(error.localizedDescription)
            }
        } else {
            serviceErrorIfAny = .statusCodeInvalid(status)
        }
        
        return (resultData, serviceErrorIfAny)
    }
    
    
    // TODO: These are still looking very similar 
    // and probably be refactored
    
    // MARK: Multi item result
    
    /// A multi item completionHandler for dataTask plus completionMethod
    func processItemsResponse(_ data: Data?,_ response: URLResponse?,_ error: Error?,
                              completion: ([[String:Any]], SearchServiceError?) -> ()) {
        
        let respResult = self.processResponse(data, response, error)
        var resultData: [[String:Any]]
        var serviceErrorIfAny: SearchServiceError?

        if let serviceError = respResult.serviceError {
            serviceErrorIfAny = serviceError
            resultData = []
        } else if let results = respResult.result["results"] as? [[String:Any]] {
            // Valid Results
            resultData = results
        } else {
            serviceErrorIfAny = .missingDataElementError("results")
            resultData = []
        }
        
        completion(resultData, serviceErrorIfAny)
    }
    
    /// A multi item completionHandler with paging for dataTask plus completionMethod
    func processPagingItemsResponse(_ data: Data?,_ response: URLResponse?,_ error: Error?,
                              completion: ([[String:Any]], SearchServiceError?, Int, Int) -> ()) {
        
        let respResult = self.processResponse(data, response, error)
        var resultData: [[String:Any]]
        var serviceErrorIfAny: SearchServiceError?
        var nextPage = 0
        var totalSearchResults = 0
        
        if let serviceError = respResult.serviceError {
            serviceErrorIfAny = serviceError
            resultData = []
        } else if let results = respResult.result["results"] as? [[String:Any]] {
            // Valid Results
            resultData = results
            
            // Obtain paging elements
            if let offset = respResult.result["offset"] as? Int,
                let pageResults = respResult.result["number_of_page_results"] as? Int,
                let totalResults = respResult.result["number_of_total_results"] as? Int,
                let limit = respResult.result["limit"] as? Int {
                totalSearchResults = totalResults
                let currentPage = (offset + pageResults) / limit
                if (offset + pageResults) < totalResults {
                    nextPage = currentPage + 1
                }
            }
            
        } else {
            serviceErrorIfAny = .missingDataElementError("results")
            resultData = []
        }
        
        completion(resultData, serviceErrorIfAny, nextPage, totalSearchResults)
    }

    
    // MARK: Single item result
    
    /// A single item completionHandler for dataTask plus completionMethod
    func processItemResponse(_ data: Data?,_ response: URLResponse?,_ error: Error?,
                             completion: ([String:Any], SearchServiceError?) -> ()) {
        
        let respResult = self.processResponse(data, response, error)
        var resultData: [String:Any]
        var serviceErrorIfAny: SearchServiceError?

        if let serviceError = respResult.serviceError {
            serviceErrorIfAny = serviceError
            resultData = [:]
        } else if let results = respResult.result["results"] as? [String:Any] {
            // Valid Results
            resultData = results
        } else {
            serviceErrorIfAny = .missingDataElementError("results")
            resultData = [:]
        }
        
        completion(resultData, serviceErrorIfAny)
    }

    
    // MARK: - API
    
    /// Conduct a search submitting a request provides errors
    /// - parameter  searchTerm: term to search for
    /// - parameter  completion: closure to call when completed
    func searchResults(for searchTerm: String, completion: @escaping ([[String:Any]], SearchServiceError?) -> ()) {
        // This method does not support paging
        let defaultKeys: [SearchServiceDataKey] = [.id,.name,.deck,.image]
        let fieldListkeyStr = defaultKeys.map { $0.rawValue}.joined(separator: ",")

        var urlComponents = URLComponents(string: baseURLString)
        urlComponents?.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "resources", value: "game"),
            URLQueryItem(name: "query", value: searchTerm),
            URLQueryItem(name: "field_list", value: fieldListkeyStr)
        ]
        
        if let targetURL = urlComponents?.url?.appendingPathComponent("search") {
            // Submit the request
            URLSession.shared.dataTask(with: targetURL, completionHandler: { (data, response, error) in
                
                self.processItemsResponse(data, response, error, completion: completion)
                
            }).resume()
        }
    }
  
    /// Conduct a search with paging, submitting a request provides errors
    /// - parameter  searchTerm: term to search for
    /// - parameter  page: page number
    /// - parameter  completion: closure to call when completed, data, err, nextpage, total
    func searchResults(for searchTerm: String, page: Int, completion: @escaping ([[String:Any]], SearchServiceError?, Int, Int) -> ()) {
        
        let defaultKeys: [SearchServiceDataKey] = [.id,.name,.deck,.image]
        let fieldListkeyStr = defaultKeys.map { $0.rawValue}.joined(separator: ",")

        var urlComponents = URLComponents(string: baseURLString)
        urlComponents?.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "resources", value: "game"),
            URLQueryItem(name: "query", value: searchTerm),
            URLQueryItem(name: "page", value: "\(page)"),
            URLQueryItem(name: "field_list", value: fieldListkeyStr)
        ]
        
        if let targetURL = urlComponents?.url?.appendingPathComponent("search") {
            // Submit the request
            URLSession.shared.dataTask(with: targetURL, completionHandler: { (data, response, error) in
                
                self.processPagingItemsResponse(data, response, error, completion: completion)
                
            }).resume()
        }
    }
    
    /// Submit a request for details
    /// - parameter  resourceIdentifier: id of resource
    /// - parameter  completion: closure to call when completed
    func details(for resourceIdentifier: Int, completion: @escaping ([String:Any], SearchServiceError?) -> ()) {
        
        let defaultKeys: [SearchServiceDataKey] = [.id,.name,.deck,.description,.image,.images]
        let fieldListkeyStr = defaultKeys.map { $0.rawValue}.joined(separator: ",")

        var urlComponents = URLComponents(string: baseURLString)
        urlComponents?.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "resources", value: "game"),
            URLQueryItem(name: "field_list", value: fieldListkeyStr)
        ]
        
        if let baseTargetURL = urlComponents?.url?.appendingPathComponent("game") {
            
            let targetURL = baseTargetURL.appendingPathComponent("\(resourceIdentifier)")
            // Submit the request
            URLSession.shared.dataTask(with: targetURL, completionHandler: { (data, response, error) in
                
                self.processItemResponse(data, response, error, completion: completion)

            }).resume()
        }
    }
    
    /// Submit a request for details with specific keys
    /// - parameter  resourceIdentifier: id of resource
    /// - parameter  completion: closure to call when completed
    /// - parameter  keys: the data fields to retireve
    func details(for resourceIdentifier: Int, retrieving keys:[SearchServiceDataKey], completion: @escaping ([String:Any], SearchServiceError?) -> ()) {
        
        let defaultKeys: [SearchServiceDataKey] = [.id]
        let allKeys = defaultKeys + keys
        let fieldListkeyStr = allKeys.map { $0.rawValue}.joined(separator: ",")
        
        var urlComponents = URLComponents(string: baseURLString)
        urlComponents?.queryItems = [
            URLQueryItem(name: "api_key", value: apiKey),
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "resources", value: "game"),
            URLQueryItem(name: "field_list", value: fieldListkeyStr)
        ]
        
        if let baseTargetURL = urlComponents?.url?.appendingPathComponent("game") {
            
            let targetURL = baseTargetURL.appendingPathComponent("\(resourceIdentifier)")
            // Submit the request
            URLSession.shared.dataTask(with: targetURL, completionHandler: { (data, response, error) in
                
                self.processItemResponse(data, response, error, completion: completion)
                
            }).resume()
        }
    }


}


//
//  http://www.giantbomb.com/api/search/?
//  api_key=ad59511e47a20c10dfcecb643954dde2e3bd48c0&format=json&query="hanks"&resources=game
//
