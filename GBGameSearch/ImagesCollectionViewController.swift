//
//  ImagesCollectionViewController.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/10/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//
/*

 This controller is first hidden and may not be accessed at all
 so when initially loaded it will not hav values.
 It needs to provide a mechanism to respond when acted upon
 One option is to have a reference detailsData and observe changes to 
 a property
 Another option is to provide a method intialLoad()
 Another option is to have the owner set the dtaa and reload
 */

import UIKit

class ScreenShotCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var screenImageView: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
}


private let reuseIdentifier = "ScreenShotCell"

class ImagesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    // MARK: Properties
    
    /// Images URL data
    var imagesDetailsData: [[String:Any]] = []
    
    /// If the images are preloaded
    var imagesPreLoaded: [UIImage]?
    
    /// A lock for synchronized access to image cache.
    let lock = NSLock()
    
    /// A cache to store image
    var imageCache:NSCache<AnyObject,UIImage> = NSCache()
    
    /// A place holder image while image is being retrieved
    lazy var placeHolderImage = UIImage(named: "blankImage")

    /// Indicates whether to use cell update
    var cellUpdateOnImageAvailable = false
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let preloaded = imagesPreLoaded {
            return preloaded.count
        } else {
            return imagesDetailsData.count
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ScreenShotCollectionViewCell
    
        // Configure the cell
        
        if let preloaded = imagesPreLoaded {
            let image = preloaded[indexPath.item]
            cell.screenImageView.image = image
        } else {
            let item = imagesDetailsData[indexPath.item]
            if let imageUrlString = imageUrlStringForItem(item: item) {
                
                cell.screenImageView.image = self.imageFor(urlString: imageUrlString) { status in
                    if status {
                        cell.activity.startAnimating()
                    } else {
                        cell.activity.stopAnimating()
                    }
                }
            }
        }
    
        return cell
    }
    
    
    // MARK: Image Processing

    /// Returns a url string for item
    /// - parameter item: data for the item
    func imageUrlStringForItem(item: [String:Any]) -> String? {
        let image_specifier: String
        if self.view.traitCollection.horizontalSizeClass == .regular &&
            self.view.traitCollection.verticalSizeClass == .regular {
            image_specifier = "super_url"
        } else {
            image_specifier = "medium_url"
        }
        return item[image_specifier] as! String?
    }
    
    
    // MARK: Caching
    
    /// Returns a cached image or placeholder
    /// - parameter urlString: a url string
    func imageFor(urlString: String, retrieving: (Bool) -> Void) -> UIImage? {
        // lock the image cache
        lock.lock(); defer { lock.unlock() }
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) {

            retrieving(false)
            return cachedImage
        } else {
            
            retrieving(true)
            /// Dispatch image retrieval to default background
            DispatchQueue.global().async {
                if let imageURL = URL(string: urlString),
                    let imageData = try? Data(contentsOf: imageURL),
                    let image = UIImage(data: imageData) {
                    self.imageAvailable(image: image, for: urlString)
                    
                    if self.cellUpdateOnImageAvailable {
                        DispatchQueue.main.async {
                            self.imageUpdate(for: urlString, with: image)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                        }
                    }
                }
            } // dispatch
            
            if let placeHolder = placeHolderImage {
                return placeHolder
            } else {
                return nil
            }
        }
    }
    
    /// Called when an image has arrived from retrieval in background
    /// - parameter image: the image that became available
    /// - parameter urlString: a url string
    func imageAvailable(image: UIImage, for urlString:String) {
        // lock the image cache
        lock.lock(); defer { lock.unlock() }
        imageCache.setObject(image, forKey: urlString as AnyObject)
    }
    
    
    // MARK: Cell Update Image
    
    /// Perform image update having the url, with image
    /// - parameter urlString: a url string for the image
    /// - parameter image: the image that will update
    func imageUpdate(for urlString: String, with image:UIImage) {

        performImageUpdateOnVisible(for: urlString)

//        performImageUpdateOnVisible(for: urlString) { cell in
//            if let screenShotCell = cell as? ScreenShotCollectionViewCell {
//                DispatchQueue.main.async {
//                    screenShotCell.screenImageView?.image = image
//                    //print("stopAnimating")
//                    screenShotCell.activity.stopAnimating()
//                }
//            }
//        }
        
    }

    /// Apply the image on visible cells
    /// - parameter urlString: a url string for the image
    func performImageUpdateOnVisible(for urlString: String) {

        if let visible = self.collectionView?.indexPathsForVisibleItems {
            var updateItems: [IndexPath] = []
            for indexPath in visible {
                let item = imagesDetailsData[indexPath.item]
                if let iconURL = imageUrlStringForItem(item: item), iconURL == urlString {
                    print("imageUpdate \(indexPath.item)")
                    updateItems.append(indexPath)
                 }
            }
            
            if !updateItems.isEmpty {
                DispatchQueue.main.async {
                    self.collectionView?.reloadItems(at: updateItems)
                }
            }
            
        } else {
            print("no visible")
        }
    }
    
    /// Apply the image on visible cells
    /// - parameter urlString: a url string for the image
    /// - parameter completion: the closure for results containing the matched visible cell
    func performImageUpdateOnVisible(for urlString: String, completion: (UICollectionViewCell) -> Void) {
        
        if let visible = self.collectionView?.indexPathsForVisibleItems {
            for indexPath in visible {
                //print("imageUpdateVis \(indexPath.item)")
                let item = imagesDetailsData[indexPath.item]
                if let iconURL = imageUrlStringForItem(item: item), iconURL == urlString {
                    if let cell = self.collectionView?.cellForItem(at: indexPath) {
                        print("imageUpdate \(indexPath.item)")
                        completion(cell)
                    }
                }
            }
        } else {
            print("no visible")
        }
    }


    
    // MARK: Cell Resizing
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        //print(#function)
        
        if self.traitCollection.horizontalSizeClass == .regular &&
            self.traitCollection.verticalSizeClass == .regular {
            print("rxr")
            self.collectionView!.isPagingEnabled = true
            if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
                let sz = self.collectionView!.bounds.size
                layout.itemSize = CGSize(width: sz.width - 10, height: sz.height - 10)
                self.collectionView!.performBatchUpdates(nil, completion:nil)
            }
        }
        
        if self.traitCollection.horizontalSizeClass == .compact &&
            self.traitCollection.verticalSizeClass == .regular {
            print("cxr")
            self.collectionView!.isPagingEnabled = false
            if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
                let sz = self.collectionView!.frame.size
                //print(sz)
                layout.itemSize = CGSize(width: sz.width - 20, height: sz.height / 2.8)
                self.collectionView!.performBatchUpdates(nil, completion:nil)
            }
        }
        
        if self.traitCollection.horizontalSizeClass == .regular &&
            self.traitCollection.verticalSizeClass == .compact {
            print("rxc")
        }
        
        if self.traitCollection.horizontalSizeClass == .compact &&
            self.traitCollection.verticalSizeClass == .compact {
            print("cxc")
            //self.collectionView!.isPagingEnabled = true
            if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
                let sz = self.collectionView!.bounds.size
                //print(sz)
                layout.itemSize = CGSize(width: sz.width - 10, height: sz.height - 10)
                self.collectionView!.performBatchUpdates(nil, completion:nil)
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        //print("\(#function) \(size)")
        // Dont have the will transition to traitCollection
        
        //        if self.traitCollection.horizontalSizeClass == .regular &&
        //            self.traitCollection.verticalSizeClass == .regular {
        //            print("rxr")
        //            self.collectionView!.isPagingEnabled = true
        //            if let layout = self.collectionViewLayout as? UICollectionViewFlowLayout {
        //                layout.itemSize = CGSize(width: size.width - 10, height: size.height - 10)
        //                self.collectionView!.performBatchUpdates(nil, completion:nil)
        //            }
        //        }
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
 
}

