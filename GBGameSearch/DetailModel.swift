//
//  DetailModel.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/7/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

import UIKit

class DetailModel: NSObject  {
    /// Title for the item
    var nameTitle: String?

    /// A description text in html
    var htmlDescription: String?
    
    /// Url for the image
    var imageURL: URL?
    
    /// Starting image if available
    var startImage: UIImage?
    
    /// Whether image is tapped to enlarge
    var imageTapped = false
    
    /// identifier of item of detail
    var resource_id: Int?
    
    /// Stored details for this item
    var details: [String:Any] = [:]
    
    /// The images screen shots data
    var imagesDetails: [[String:Any]]?
    
    /// Images of the resource
    var images: [UIImage] = []
    
    /// Indicate whether anim images are loaded
    var imagesAnimationEnabled = false
}
