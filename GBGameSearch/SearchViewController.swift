//
//  ViewController.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/3/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// ------------------------------------------------------------
///
///  Home view controller
///  Uses a cache NSCache to store image icons
///    is protected by locks so that writes to it
///    can happen asynchronously as reads also are happening
///  Maintains a reference to the Search Service and 
///    handles all requests to the service
///  Manages the interface with the Saerch textfield and 
///    search button enablement
///
// ------------------------------------------------------------

import UIKit
import os.log

/// Callback with update
/// - parameter  Bool: success or fail
/// - parameter  data: dictionary of data
typealias SearchServiceUpdateDetailsCallback = (Bool, [String:Any]) -> Void

/// Callback with update
/// - parameter  Bool: success or fail
/// - parameter  data: array of dictionary results
typealias SearchServiceSearchResultsCallback = (Bool, [[String:Any]], Int) -> Void


protocol SearchServiceProtocol: class {
    func searchServiceDetailsFor(_ identifier: Int, update: @escaping SearchServiceUpdateDetailsCallback)
    func searchServiceResultsFor(_ page: Int, searchTerm: String, update: @escaping SearchServiceSearchResultsCallback)
    func searchServiceDetailsDescriptionFor(_ identifier: Int, update: @escaping SearchServiceUpdateDetailsCallback)
    func searchServiceDetailsImagesFor(_ identifier: Int, update: @escaping SearchServiceUpdateDetailsCallback)
}


/// Home view controller for Search
class SearchViewController: UIViewController, SearchTableDelegate, UITextFieldDelegate, SearchServiceProtocol {

    /// UIElements
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    
    /// A service object to make the request
    lazy var searchService = GameSearchService()
    
    /// A place holder image while image is being retrieved
    lazy var placeHolderImage = UIImage(named: "blankImage")
    
    /// The result of initial search
    var searchResults: [[String:Any]]?
    
    /// The next page optained from initial search
    var nextPage = 0
    
    /// The total reaults of search
    var totalResults = 0

    /// A reference to the search results table
    weak var searchResultsTable: SearchResultsTableViewController?
    
    /// A lock for synchronized access to image cache.
    let lock = NSLock()
    
    /// A cache to store image
    var imageCache:NSCache<AnyObject,UIImage> = NSCache()
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        imageCache.countLimit = 100
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        imageCache.removeAllObjects()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchButtonEnablement()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Cleanup specifically when coming back from search results
        DispatchQueue.global().async {
            self.cleanChache()
        }
    }


    // MARK: HELPER

    /// Set enablement based on text in textfield
    func searchButtonEnablement() {
        if let searchTerm = searchTextField.text, !searchTerm.isEmpty {
            searchButton.isEnabled = true
        } else {
            searchButton.isEnabled = false
        }
    }

    /// Clean cache with lock
    func cleanChache() {
        lock.lock(); defer { lock.unlock() }
        imageCache.removeAllObjects()
    }
  
    
    // MARK: UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let searchTerm = textField.text, !searchTerm.isEmpty {
            performSearch(for: searchTerm)
        } else {
            print("MUST ENTER TEXT")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let searchTerm = textField.text, !searchTerm.isEmpty {
            textField.resignFirstResponder()
            return true
        } else {
            return false
        }
    }

    func textDidChange(_ textField: UITextField) {
        searchButtonEnablement()
    }

    
    // MARK: Action
    
    @IBAction func searchTapped(_ sender: UIButton) {
        if searchTextField.isFirstResponder {
            searchTextField.resignFirstResponder()
            // Resign will textFieldDidEndEditing and performSearch
        } else {
            if let searchTerm = searchTextField.text, !searchTerm.isEmpty {
                performSearch(for: searchTerm)
            } else {
                fatalError("Search button should not be enabled")
            }
        }
    }

    
    // MARK: SearchTableDelegate

    /// Returns the image for the requested url
    func resultsTable(_ controller:SearchResultsTableViewController, imageFor url:String) -> UIImage? {
        return imageFor(urlString: url)
    }
    
    
    // MARK: Initial Search
    
    /// Perform the search
    func performSearch(for searchTerm: String) {
        searchButton.isEnabled = false
        activity.startAnimating()
        
        searchService.searchResults(for: searchTerm, page: 1) { (results, serviceError, nextPage, total) in
            DispatchQueue.main.async {
                self.activity.stopAnimating()
            }
            if let serviceError = serviceError {
                self.handleResponseError(error: serviceError)
            } else {
                // Valid results
                if results.isEmpty {
                    self.informationAlert("No Results")
                } else {
                    // All is good, let's continue
                    self.searchResults = results
                    self.nextPage = nextPage
                    self.totalResults = total
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "PresentSearchResults" , sender: self)
                    }
                }
            }
            
        } // completion
    }

    
    // MARK: Caching
    
    /// Returns a cached image or placeholder
    /// - parameter urlString: a url string
    func imageFor(urlString: String) -> UIImage? {
        // lock the image cache
        lock.lock(); defer { lock.unlock() }
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) {
            return cachedImage
        } else {
            /// Dispatch image retrieval to default background
            DispatchQueue.global().async {
                if let imageURL = URL(string: urlString),
                    let imageData = try? Data(contentsOf: imageURL),
                    let image = UIImage(data: imageData) {
                    self.imageAvailable(image: image, for: urlString)
                    
                    DispatchQueue.main.async {
                        // Both methods work, the second doesnt call back requesting the image
                        // because it is passed in
                        //self.searchResultsTable?.imageUpdate(for: urlString)
                        self.searchResultsTable?.imageUpdate(for: urlString, with: image)
                    }
                }
            } // dispatch
            
            if let placeHolder = placeHolderImage {
                return placeHolder
            } else {
                return nil
            }
        }
    }
    
    /// Called when an image has arrived from retrieval in background
    func imageAvailable(image: UIImage,for urlString:String) {
        // lock the image cache
        lock.lock(); defer { lock.unlock() }
        imageCache.setObject(image, forKey: urlString as AnyObject)
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "PresentSearchResults" {
            if let resultsTable = segue.destination as? SearchResultsTableViewController {
                if let searchResults = searchResults {
                    resultsTable.searchResults = searchResults
                    self.searchResults?.removeAll() // Dont need duplicate data
                }
                
                if let searchTerm = searchTextField.text, !searchTerm.isEmpty {
                    resultsTable.searchTerm = searchTerm
                }

                resultsTable.searchService = self
                resultsTable.totalResults = self.totalResults
                resultsTable.nextPage = self.nextPage
                resultsTable.delegate = self
                // keep a reference for image updates
                searchResultsTable = resultsTable
            }
        }
    }
    
}



// MARK: SearchServiceProtocol

extension SearchViewController {
    
    func searchServiceDetailsFor(_ identifier: Int, update: @escaping SearchServiceUpdateDetailsCallback) {
        
        searchService.details(for: identifier) { ( results, serviceError ) in
            var resultStatus = false
            let resultData: [String:Any]
            if let serviceError = serviceError {
                self.handleResponseError(error: serviceError)
                resultData = [:]
            } else {
                // Valid results
                resultData = results
                resultStatus = true
            }
            DispatchQueue.main.async {
                update(resultStatus, resultData)
            }
        }
    }
    
    func searchServiceDetailsDescriptionFor(_ identifier: Int, update: @escaping SearchServiceUpdateDetailsCallback) {
        
        searchService.details(for: identifier, retrieving:[.description]) { ( results, serviceError ) in
            var resultStatus = false
            let resultData: [String:Any]
            if let serviceError = serviceError {
                self.handleResponseError(error: serviceError)
                resultData = [:]
            } else {
                // Valid results
                resultData = results
                resultStatus = true
                print(Array(results.keys))
            }
            DispatchQueue.main.async {
                update(resultStatus, resultData)
            }
        }
    }
    
    func searchServiceDetailsImagesFor(_ identifier: Int, update: @escaping SearchServiceUpdateDetailsCallback) {
        
        searchService.details(for: identifier, retrieving:[.images]) { ( results, serviceError ) in
            var resultStatus = false
            let resultData: [String:Any]
            if let serviceError = serviceError {
                self.handleResponseError(error: serviceError)
                resultData = [:]
            } else {
                // Valid results
                resultData = results
                resultStatus = true
                print(Array(results.keys))
            }
            DispatchQueue.main.async {
                update(resultStatus, resultData)
            }
        }
    }
    
    func searchServiceResultsFor(_ page: Int, searchTerm: String, update: @escaping SearchServiceSearchResultsCallback) {
        
        searchService.searchResults(for: searchTerm, page: page) { (results, serviceError, nextPage, totalResults) in
            var resultStatus = false
            let resultData: [[String:Any]]
            var resultNextPage = page
            if let serviceError = serviceError {
                self.handleResponseError(error: serviceError)
                resultData = []
            } else {
                // Valid results
                if results.isEmpty {
                    self.informationAlert("No Results")
                    resultData = []
                } else {
                    // All is good, let's continue
                    resultStatus = true
                    resultData = results
                    resultNextPage = nextPage
                }
            }
            update(resultStatus, resultData, resultNextPage)
        }
    }

}


// MARK: Error Handler and Alerts

extension SearchViewController {
    
    func handleResponseError(error: GameSearchService.SearchServiceError) {
        switch error {
        case .missingDataElementError(let msg) :
            self.unableToProcessResponseAlert()
            if #available(iOS 10.0, *) {
                os_log(".missingDataElementError [%s]", log: OSLog.default, type: .debug, msg)
            } else {
                print(".missingDataElementError \(msg)")
            }
        case .missingDataError :
            self.unableToProcessResponseAlert()
            if #available(iOS 10.0, *) {
                os_log(".missingDataError", log: OSLog.default, type: .debug)
            } else {
                print(".missingDataError ")
            }
        case .unrecognizedDataFormat:
            self.unableToProcessResponseAlert()
            if #available(iOS 10.0, *) {
                os_log(".unrecognizedDataFormat", log: OSLog.default, type: .debug)
            } else {
                print(".unrecognizedDataFormat ")
            }
        case .parsingError(let msg) :
            self.unableToProcessResponseAlert()
            if #available(iOS 10.0, *) {
                os_log(".parsingError [%s]", log: OSLog.default, type: .debug, msg)
            } else {
                print(".parsingError \(msg)")
            }
        case .responseError(let error) :
            self.errorAlert("Response Error\n\(error.code) \(error.localizedDescription)")
        case .statusCodeInvalid(let statusCode) :
            self.unableToProcessResponseAlert()
            if #available(iOS 10.0, *) {
                os_log(".statusCodeInvalid [%d]", log: OSLog.default, type: .debug, statusCode)
            } else {
                print(".statusCodeInvalid \(statusCode)")
            }
        case .apiStatusCode(let code, let msg) where code == 101 || code == 105:
            self.errorAlert("Api Status Code\n\(msg)")
        case .apiStatusCode(let code, let msg):
            self.errorAlert("Api Status Code \(code)")
            print("Api Status Code\n\(msg)")
        }
    }
    
    /// Present an error alert
    /// - parameter  message: message to display in the alert
    func errorAlert(_ message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            self.searchButtonEnablement()
        }
        alertController.addAction(okAction)
        if let navigationController = self.navigationController {
            navigationController.present(alertController, animated: true, completion: nil)
        } else {
            present(alertController, animated: true, completion: nil)
        }
    }
    
    /// Present an information alert
    /// - parameter  message: message to display in the alert
    func informationAlert(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            self.searchButtonEnablement()
        }
        alertController.addAction(okAction)
        if let navigationController = self.navigationController {
            navigationController.present(alertController, animated: true, completion: nil)
        } else {
            present(alertController, animated: true, completion: nil)
        }
    }
    
    /// Present an static alert 'UnableResponse'
    func unableToProcessResponseAlert() {
        let alertController = UIAlertController(title: nil, message: "Unable to process response", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            self.searchButtonEnablement()
        }
        alertController.addAction(okAction)
        if let navigationController = self.navigationController {
            navigationController.present(alertController, animated: true, completion: nil)
        } else {
            present(alertController, animated: true, completion: nil)
        }
    }
    
}

