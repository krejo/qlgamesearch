//
//  GameSearchServiceTests.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/6/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// ------------------------------------------------------------
///
///  Primarily test error handling with SearchServiceError
///  There are three propcess Methods
///
///  processItemResponse - a single item
///  processPagingItemsResponse - array of items
///  processItemsResponse - array of items
///
///  The difference between processPagingItemsResponse and processItemsResponse is that one
///  processes the paging information where the other does not
///
///  Tests are focused on the first two processItemResponse and processPagingItemsResponse
///  The single item response is used when requesting detail
///  The pagingItems method is used for search results
///
// ------------------------------------------------------------


// Nice helper methods (Expectation on enums) from
// http://www.obqo.de/blog/2015/10/31/testing-enumerations-made-easy/
//

import XCTest
@testable import GBGameSearch

class GameSearchServiceTests: XCTestCase {
    
    var sut: GameSearchService!
    
    override func setUp() {
        super.setUp()
        sut = GameSearchService()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    
    // MARK: - Service Error Testcases
    
    /// nil data, nil urlResp, nil error
    func testNoData() {
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(nil, nil, nil) { (result, serviceError) in
            status = 1
            expectMissingData(actual: serviceError)
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(nil, nil, nil) { (result, serviceError, next, total) in
            status = 1
            expectMissingData(actual: serviceError)
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // Items
        status = 0
        sut.processItemsResponse(nil, nil, nil) { (result, serviceError) in
            status = 1
            expectMissingData(actual: serviceError)
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// some data, nil urlResp, nil error
    func testNoResponse() {
        let data = Data()
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(data, nil, nil) { (result, serviceError) in
            status = 1
            expectStatusCode(actual: serviceError) { code in
                XCTAssertEqual(code, 0, "code should be 0")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(data, nil, nil) { (result, serviceError, next, total) in
            status = 1
            expectStatusCode(actual: serviceError) { code in
                XCTAssertEqual(code, 0, "code should be 0")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// nil data, nil urlResp, some error
    func testErrorFromResponse() {
        let respError = NSError.init(domain: "test", code: 2, userInfo: nil)
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(nil, nil, respError) { (result, serviceError) in
            status = 1
            expectResponseError(actual: serviceError) { error in
                XCTAssertEqual(error.code, 2, "code should be 2")
                XCTAssertEqual(error.domain, "test", "domain should be 'test'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(nil, nil, respError) { (result, serviceError, next, total) in
            status = 1
            expectResponseError(actual: serviceError) { error in
                XCTAssertEqual(error.code, 2, "code should be 2")
                XCTAssertEqual(error.domain, "test", "domain should be 'test'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }

    /// nil data, nil urlResp, some error
    func testErrorFromResponseWithValidResponse() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let respError = NSError.init(domain: "test", code: 2, userInfo: nil)
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(nil, urlResponse, respError) { (result, serviceError) in
            status = 1
            expectResponseError(actual: serviceError) { error in
                XCTAssertEqual(error.code, 2, "code should be 2")
                XCTAssertEqual(error.domain, "test", "domain should be 'test'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(nil, urlResponse, respError) { (result, serviceError, next, total) in
            status = 1
            expectResponseError(actual: serviceError) { error in
                XCTAssertEqual(error.code, 2, "code should be 2")
                XCTAssertEqual(error.domain, "test", "domain should be 'test'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }

    
    /// Invalid urlResp 300, valid jsonData
    func testBadStatus() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 300, httpVersion: "http/1.1", headerFields: nil)
        let rData: [String:Any] = ["status_code":101, "error":"Foo"]
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectStatusCode(actual: serviceError) { code in
                XCTAssertEqual(code, 300,  "code should be 300")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            expectStatusCode(actual: serviceError) { code in
                XCTAssertEqual(code, 300,  "code should be 300")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// Valid urlResp 200, valid json, api status_code not ok
    func testBadApiStatus() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let rData: [String:Any] = ["status_code":101,"error":"Foo"] // invalid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectApiStatusCode(actual: serviceError) { code, msg in
                XCTAssertEqual(code, 101, "code should be 101")
                XCTAssertEqual(msg, "Foo", "message should be 'Foo'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            expectApiStatusCode(actual: serviceError) { code, msg in
                XCTAssertEqual(code, 101, "code should be 101")
                XCTAssertEqual(msg, "Foo", "message should be 'Foo'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
  
    /// Valid urlResp 200, invalid json
    func testJsonParseError() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let jsonStr = "{\"status_code\":101,\"error\":\"Foo"  // invalid json
        let jsonData = jsonStr.data(using: .utf8)
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectParsingError(actual: serviceError) { msg in
                print(msg)
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            expectParsingError(actual: serviceError) { msg in
                print(msg)
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // Items
        status = 0
        sut.processItemsResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectParsingError(actual: serviceError) { msg in
                print(msg)
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// Valid urlResp 200, valid json, missing 'results' element
    func testMissingElementResults() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let rData: [String:Any] = ["status_code":1,"error":"Foo"] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectMissingDataElementError(actual: serviceError) { msg in
                XCTAssertEqual(msg, "results",  "the associated value should contain the missinge element 'results'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            expectMissingDataElementError(actual: serviceError) { msg in
                XCTAssertEqual(msg, "results", "the associated value should contain the missinge element 'results'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// Valid urlResp 200, valid json, missing 'status_code' element
    func testMissingElementStatusCode() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http1.1", headerFields: nil)
        let rData: [String:Any] = ["statuX_Xode":1,"error":"Foo"] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        // Item
        status = 0
        sut.processItemResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectMissingDataElementError(actual: serviceError) { msg in
                XCTAssertEqual(msg, "status_code", "the associated value should contain the missinge element 'status_code'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            expectMissingDataElementError(actual: serviceError) { msg in
                XCTAssertEqual(msg, "status_code", "the associated value should contain the missinge element 'status_code'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // Items
        status = 0
        sut.processItemsResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            expectMissingDataElementError(actual: serviceError) { msg in
                XCTAssertEqual(msg, "status_code", "the associated value should contain the missinge element 'status_code'")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }

    
    // MARK: - Helpers
    
    /// Helper missingDataError
    func expectMissingData(
        actual: GameSearchService.SearchServiceError?,
        file: String = #file, line: UInt = #line,
        test: () -> () = { _ in } ) {
        guard let err = actual else { XCTFail("expected serviceError should be set and is not, in file \(file) at \(line)"); return }
        
        guard case .missingDataError = err else {
            XCTFail("expected <missingDataError>, got<\(err)> in file \(file) at \(line)"); return
        }
        test()
    }
    /// Helper apiStatusCode
    func expectApiStatusCode(
        actual: GameSearchService.SearchServiceError?,
        file: String = #file, line: UInt = #line,
        test: (Int,String) -> () = { _ in } ) {
        guard let err = actual else { XCTFail("expected serviceError should be set and is not, in file \(file) at \(line)"); return }
        
        guard case let .apiStatusCode(code, msg) = err else {
            XCTFail("expected <apiStatusCode>, got<\(err)> in file \(file) at \(line)"); return
        }
        test(code,msg)
    }
    /// Helper parsingError
    func expectParsingError(
        actual: GameSearchService.SearchServiceError?,
        file: String = #file, line: UInt = #line,
        test: (String) -> () = { _ in } ) {
        guard let err = actual else { XCTFail("expected serviceError should be set and is not, in file \(file) at \(line)"); return }
        
        guard case let .parsingError(msg) = err else {
            XCTFail("expected <parsingError>, got<\(err)> in file \(file) at \(line)"); return
        }
        test(msg)
    }
    /// Helper statusCodeInvalid
    func expectStatusCode(
        actual: GameSearchService.SearchServiceError?,
        file: String = #file, line: UInt = #line,
        test: (Int) -> () = { _ in } ) {
        guard let err = actual else { XCTFail("expected serviceError should be set and is not, in file \(file) at \(line)"); return }
        
        guard case let .statusCodeInvalid(code) = err else {
            XCTFail("expected <statusCodeInvalid>, got<\(err)> in file \(file) at \(line)"); return
        }
        test(code)
    }
    /// Helper missingDataElementError
    func expectMissingDataElementError(
        actual: GameSearchService.SearchServiceError?,
        file: String = #file, line: UInt = #line,
        test: (String) -> () = { _ in } ) {
        guard let err = actual else { XCTFail("expected serviceError should be set and is not, in file \(file) at \(line)"); return }
        
        guard case let .missingDataElementError(msg) = err else {
            XCTFail("expected <missingDataElementError>, got<\(err)> in file \(file) at \(line)"); return
        }
        test(msg)
    }
    /// Helper responseError
    func expectResponseError(
        actual: GameSearchService.SearchServiceError?,
        file: String = #file, line: UInt = #line,
        test: (NSError) -> () = { _ in } ) {
        guard let err = actual else { XCTFail("expected serviceError should be set and is not, in file \(file) at \(line)"); return }
        
        guard case let .responseError(nsError) = err else {
            XCTFail("expected <responseError>, got<\(err)> in file \(file) at \(line)"); return
        }
        test(nsError)
    }


}
