//
//  GameSearchServiceResultsTests.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/8/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// ------------------------------------------------------------
///
///  Primarily test valid results from serachService
///
// ------------------------------------------------------------

import XCTest
@testable import GBGameSearch

class GameSearchServiceResultsTests: XCTestCase {
    
    var sut: GameSearchService!
    
    override func setUp() {
        super.setUp()
        sut = GameSearchService()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Valid Results
    
    /// Valid urlResp 200, valid json, valid results
    func testValidResults() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let result: [String:Any] = ["name" : "bar"]
        let results: [[String:Any]] = [result]
        let rData: [String:Any] = ["status_code":1,"error":"Foo",
                                   "offset":0,
                                   "number_of_page_results":1,
                                   "number_of_total_results":1,
                                   "limit":1,
                                   "results":results] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        // PagingItems
        status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            if let _ = serviceError {
                XCTFail("serviceError should not have a value")
            } else {
                XCTAssertEqual(total, 1, "total should be 1")
                XCTAssertEqual(next, 0, "next should be 0")
                XCTAssertEqual(result.count, 1,"should have count of 1")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
        // Items
        status = 0
        sut.processItemsResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            if let _ = serviceError {
                XCTFail("serviceError should not have a value")
            } else {
                XCTAssertEqual(result.count, 1,"should have count of 1")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// Valid urlResp 200, valid json, valid single result
    func testSingleResult() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let expectedResult: [String:Any] = ["name" : "bar"]
        let rData: [String:Any] = ["status_code":1,"error":"Foo","results":expectedResult] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        sut.processItemResponse(jsonData, urlResponse, nil) { (result, serviceError) in
            status = 1
            if let _ = serviceError {
                XCTFail("serviceError should not have a value")
            } else {
                XCTAssertEqual(result["name"] as? String, expectedResult["name"] as? String)
                XCTAssertEqual(result.count, 1,"should have count of 1")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
}
