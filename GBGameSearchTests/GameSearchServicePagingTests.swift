//
//  GameSearchServicePagingTests.swift
//  GBGameSearch
//
//  Created by Joe Ker on 3/8/17.
//  Copyright © 2017 Joe Ker. All rights reserved.
//

// ------------------------------------------------------------
///
///  Primarily test paging mechanism
///
// ------------------------------------------------------------

import XCTest
@testable import GBGameSearch

class GameSearchServicePagingTests: XCTestCase {
    
    var sut: GameSearchService!
    
    override func setUp() {
        super.setUp()
        sut = GameSearchService()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Paging
    
    /// Valid urlResp 200, valid json, valid results, paging
    func testPagingResultsBorder() {
        // Exactly one more full page
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let results: [[String:Any]] = [["name" : "bar"]]
        let rData: [String:Any] = ["status_code":1,"error":"Foo",
                                   "offset":0,
                                   "number_of_page_results":10,
                                   "number_of_total_results":20,
                                   "limit":10,
                                   "results":results] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            if let _ = serviceError {
                XCTFail("serviceError should not have a value")
            } else {
                XCTAssertEqual(total, 20,"total should be 20")
                XCTAssertEqual(next, 2,"next should be 2")
                XCTAssertEqual(result.count, 1,"should have count of 1")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// Valid urlResp 200, valid json, valid results, paging
    func testPagingResultsPartialNext() {
        // Next page is partial one more full page
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let results: [[String:Any]] = [["name" : "bar"]]
        let rData: [String:Any] = ["status_code":1,"error":"Foo",
                                   "offset":0,
                                   "number_of_page_results":10,
                                   "number_of_total_results":16,
                                   "limit":10,
                                   "results":results] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            if let _ = serviceError {
                XCTFail("serviceError should not have a value")
            } else {
                XCTAssertEqual(total, 16, "total should be 16")
                XCTAssertEqual(next, 2, "next should be 2")
                XCTAssertEqual(result.count, 1,"should have count of 1")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
    /// Valid urlResp 200, valid json, valid results, paging no next
    func testPagingFullResults() {
        let urlResponse = HTTPURLResponse(url: URL(string:"/")!, statusCode: 200, httpVersion: "http/1.1", headerFields: nil)
        let results: [[String:Any]] = [["name" : "bar"]]
        let rData: [String:Any] = ["status_code":1,"error":"Foo",
                                   "offset":0,
                                   "number_of_page_results":2,
                                   "number_of_total_results":2,
                                   "limit":2,
                                   "results":results] // valid status_code
        let jsonData = try! JSONSerialization.data(withJSONObject: rData, options: [])
        var status = 0
        sut.processPagingItemsResponse(jsonData, urlResponse, nil) { (result, serviceError, next, total) in
            status = 1
            if let _ = serviceError {
                XCTFail("serviceError should not have a value")
            } else {
                XCTAssertEqual(total, 2, "total should be 2")
                XCTAssertEqual(next, 0, "next should be 0")
                XCTAssertEqual(result.count, 1,"should have count of 1")
            }
        }
        XCTAssertEqual(status, 1, "completion should have been called")
    }
    
}
